package com.nyc.nycschoolproject.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.nyc.nycschoolproject.network.model.School;
import com.nyc.nycschoolproject.network.model.SchoolDetails;
import com.nyc.nycschoolproject.network.repository.NycSchoolRepo;

import java.util.List;

public class NycViewModel extends ViewModel {

    NycSchoolRepo mNycSchoolRepo;
    public void init(){
        mNycSchoolRepo =  NycSchoolRepo.getInstance();
    }
    public MutableLiveData<List<School>> requestToFetchSchools(){
        return mNycSchoolRepo.getListOfSchools();
    }

    public MutableLiveData<List<SchoolDetails>> requestToFetchSchoolDetails(String dbn){
        return mNycSchoolRepo.getSchoolDetails(dbn);
    }
}
