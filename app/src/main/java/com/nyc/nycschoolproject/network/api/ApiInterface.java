package com.nyc.nycschoolproject.network.api;

import com.nyc.nycschoolproject.network.model.School;
import com.nyc.nycschoolproject.network.model.SchoolDetails;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("s3k6-pzi2.json?$limit=10")
    Call<List<School>> doListOfSchools();

    @GET("f9bf-2cp4.json?")
    Call<List<SchoolDetails>> doGetSchoolDetails(@Query("dbn") String dbn);
}
